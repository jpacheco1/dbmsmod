package jan.dbmsmod;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jan.dbmsmod.lists.ItemList;
import jan.dbmsmod.tools.DatabaseRemote;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("dbmsmod")
public class DatabaseMod {
	
	public static DatabaseMod instance;
	public static String modid = "dbmsmod";
	private static final Logger logger = LogManager.getLogger(modid);
	
	public DatabaseMod() {
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistration);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	private void setup(final FMLCommonSetupEvent event) {
		
	}
	
	private void clientRegistration(final FMLClientSetupEvent event) {
			
	}
	
	@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
	public static class RegistryEvents {
		
		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event) {
			event.getRegistry().registerAll(
					ItemList.database_remote = new DatabaseRemote(new Item.Properties().group(ItemGroup.TOOLS)).setRegistryName(new ResourceLocation(modid, "database_remote"))	
			);
			
		}
		
	}
}
