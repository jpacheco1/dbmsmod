package jan.dbmsmod.gui;
import com.mojang.blaze3d.platform.GlStateManager;

import jan.dbmsmod.DatabaseMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

public class GuiDatabaseRemote2 extends Screen {

	private static final int WIDTH = 179;
    private static final int HEIGHT = 208;
    
    private ResourceLocation gui = new ResourceLocation(DatabaseMod.modid, "textures/gui/database_remote_gui2.png");
    
	protected GuiDatabaseRemote2() {
		super(new StringTextComponent("Connect to a database"));
	}
	
	@Override
	protected void init() {
		int relX = (this.width - WIDTH) / 2;
		int relY = (this.height - HEIGHT) / 2;
	}
	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	@Override
	public void render(int mouseX,int mouseY,float partialTicks) {
		GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.renderBackground();
        this.minecraft.getTextureManager().bindTexture(gui);
        int relX = (this.width - WIDTH) / 2;
        int relY = (this.height - HEIGHT) / 2;
        this.blit(relX, relY, 0, 0, WIDTH, HEIGHT);
        super.render(mouseX, mouseY, partialTicks);
	}
	
	public static void open() {
        Minecraft.getInstance().displayGuiScreen(new GuiDatabaseRemote2());
    }
}
