package jan.dbmsmod.tools;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.items.IItemHandler;
import java.sql.*;
import java.util.ArrayList;

import jan.dbmsmod.lists.ItemList;

enum clickState{
	FIRST, ADD_TO_INVENTORY, ADD_TO_DATABASE
}

public class DatabaseRemote extends Item implements IItemHandler {
	Connection con = null;
	clickState flip = clickState.FIRST;
	public ArrayList<ItemData> itemsFromDB = new ArrayList<ItemData>();
	public DatabaseRemote(Properties properties) {
		super(properties);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		
		String username = playerIn.getName().getString();
		if(!playerIn.world.isRemote) {
			connectToDatabase();
				switch (flip) {
				case FIRST:
					if(checkDBForPlayer(playerIn)) {
						flip = clickState.ADD_TO_DATABASE;
					}else{
						addUserToDatabase(playerIn,username);
						flip = clickState.ADD_TO_DATABASE;
					}				
					break;
				case ADD_TO_INVENTORY:
					itemsFromDB = getDBItem(username);
					addItemsToInventory(playerIn, itemsFromDB);
					flip = clickState.ADD_TO_DATABASE;
					break;
				case ADD_TO_DATABASE:
					addItemsToDB(playerIn,removeInventoryItemsFromPlayer(playerIn), username);
					flip = clickState.ADD_TO_INVENTORY;
					break;
				}			
			closeDBConnection();
		}
				
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	public void connectToDatabase() {
		try {
			con = DriverManager.getConnection(
			"jdbc:sqlite:C:/sqlite/minecraftdatabase.db");
			System.out.println("We're in.");		
		}catch(SQLException e) {
			System.out.println(e);
		}
	}
	
	public void closeDBConnection() {
		try {	
		if(con != null) {
			con.close();
		}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addUserToDatabase(PlayerEntity p, String name){
		try {
		Statement stmt = null;
		if(!searchDatabasePlayers(name)) {
			String sql = "INSERT INTO Players(username)"
					+ "VALUES('" + name +"');";
			stmt = con.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
			sendPlayerMessage(p, "User " + name + " registered in database", TextFormatting.GOLD);
		}else {
			sendPlayerMessage(p, "User " + name + " already in database!", TextFormatting.GOLD);
		}
		} catch (SQLException e) {
			  System.out.println("Error adding user " + name + " to database.");	
		}
	}
	public boolean searchDatabasePlayers(String name){
		String result = "";
		try {
			Statement stmt2 = con.createStatement();
			ResultSet rs;
			rs = stmt2.executeQuery("SELECT username FROM Players WHERE username = '" + name + "'");
			while (rs.next()) {
				result = rs.getString("username");
			}
			stmt2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(result == "") {
			return false;
		}else {
			return true;
		}
	}
	//currently just gets last item by id
	public ArrayList<ItemData> getDBItem(String username){
		ArrayList<ItemData> itemIds = new ArrayList<ItemData>();
		
		try {
		Statement stmt3 = con.createStatement();
		ResultSet rs;
		rs = stmt3.executeQuery("SELECT id,quantity FROM has_entity WHERE username = '" + username + "'");
		while (rs.next()) {
			itemIds.add(new ItemData(rs.getString("id"),Integer.parseInt(rs.getString("quantity"))));
		}
		deleteItemsFromDB(itemIds, username);
		stmt3.close();
		System.out.println("getDBITEM: " + itemIds.get(0));
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return itemIds;
	}
	
	/**
	 * Takes in an ArrayList of ItemData and then removes those items from the external database.
	 * @param items A list of items to remove from the database
	 * @param username The players username
	 * @return true if successful
	 */
	public boolean deleteItemsFromDB(ArrayList<ItemData> items, String username) {
		try {
			Statement stmt = con.createStatement();
			for(ItemData i : items) {
				stmt.executeUpdate("DELETE FROM has_entity WHERE username = '" + username + "' AND id = " + i.id);
				System.out.println("Deleted item " + i.id + " from " + username + "'s database.");
			}		
			stmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return false;
			}
		return true;
	}
	
	/**
	 * Takes in ArrayList of ItemData and username and inserts items into database
	 * @param items items in users inventory
	 * @param username username of player to add items to
	 * @return true if successful
	 */
	public boolean addItemsToDB(PlayerEntity p, ArrayList<ItemData> items, String username) {
		try {
			Statement stmt = con.createStatement();
			for(ItemData i : items) {
				if(i.id != Integer.toString(Item.getIdFromItem(ItemList.database_remote))) {
					stmt.executeUpdate("INSERT INTO has_entity " +
					"VALUES ('" + username + "', '" + i.id + "', " + i.quantity + ")");				
					System.out.println("Added item " + i.id + " to " + username + "'s database.");
				}
				
			}	
			sendPlayerMessage(p, items.size() + " items put into database!", TextFormatting.GOLD);
			stmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
				return false;
			}
		return true;
	}
	
	/**
	 * Takes in player and returns an ArrayList of ItemData for use in adding items from user
	 * inventory to database. Removes all items from inventory other than database_remote
	 * @param playerIn current player
	 * @return items ArrayList of ItemData
	 */
	public ArrayList<ItemData> removeInventoryItemsFromPlayer(PlayerEntity playerIn){
		ArrayList<ItemData> items = new ArrayList<ItemData>();
		for(int i = 0; i < playerIn.inventory.getSizeInventory(); i++) {
			if(!playerIn.inventory.getStackInSlot(i).isEmpty() && Item.getIdFromItem(playerIn.inventory.getStackInSlot(i).getItem()) != getIdFromItem(ItemList.database_remote)) {
				items.add(new ItemData(Integer.toString(Item.getIdFromItem(playerIn.inventory.getStackInSlot(i).getItem())),playerIn.inventory.getStackInSlot(i).getCount()));
				playerIn.inventory.removeStackFromSlot(i);
			}
		}
		return items;
	}
	/**
	 * Adds items from database to selected player's inventory.
	 * @param playerIn player to add items from db to.
	 * @param iData Contains data about all items from database that user has.
	 * @return
	 */
	public boolean addItemsToInventory(PlayerEntity playerIn, ArrayList<ItemData> iData) {
		int i = 0;
		for(i = 0; i < iData.size(); i++) {
			ItemStack stack = null;
			stack = new ItemStack(Item.getItemById(Integer.parseInt(iData.get(i).id)),iData.get(i).quantity);
			if(Item.getIdFromItem(playerIn.inventory.getStackInSlot(i).getItem()) != getIdFromItem(ItemList.database_remote)) {
				playerIn.inventory.setInventorySlotContents(i, stack);	
			}	
		}	
		sendPlayerMessage(playerIn, i + " items extracted from database!", TextFormatting.GOLD);
		return true;
	}
	/**
	 * Sends a colored message in the in-game chat window to specified player p.
	 * @param p player to send message to
	 * @param message message to send
	 * @param color color of message
	 */
	public void sendPlayerMessage(PlayerEntity p, String message,TextFormatting color) {
		p.sendMessage(new StringTextComponent(message).setStyle(new Style().setColor(color)));
	}
	
	/**
	 * Checks if database contains a specified user. Returns true if user exists and false otherwise.
	 * @param p player to look for
	 * @return true if player exists in Players table
	 */
	public boolean checkDBForPlayer(PlayerEntity p) {
		if(sqlQuery("SELECT username FROM Players WHERE username = '" + p.getName().getString() + "'", "username").isEmpty()) {
			return true;
		}		
		return false;
	}
	public ArrayList<String> sqlQuery(String query, String column){
		Statement stmt2 = null;
		ResultSet rs = null;
		ArrayList<String> queryOutput = new ArrayList<String>();
		try {
			stmt2 = con.createStatement();
			rs = stmt2.executeQuery(query);
			while (rs.next()) {
				queryOutput.add(rs.getString(column));
			}
			stmt2.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return queryOutput;	
	}

	@Override
	public int getSlots() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSlotLimit(int slot) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isItemValid(int slot, ItemStack stack) {
		// TODO Auto-generated method stub
		return false;
	}
}
